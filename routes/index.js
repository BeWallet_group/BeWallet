var express = require('express');
var web3 = require('web3');
var mysql = require('../common/data/mysql.js');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    var results = mysql.examples();

    res.render('index', { title: 'Express' });
});

router.get('/auth', function(req, res, next) {
    res.render('wallet', { title: 'Hello World' });
});

router.get('/wallet', function(req, res, next) {
    res.json({
        web3_version: web3.version,
        success: true,
        message: 'Enjoy your token!',
        token: "token"
    });
});


function generate_seed() {

}


module.exports = router;
