
var mysql = require('mysql');

var TEST_DATABASE = 'test1'; //连接的数据库名

//创建连接
var client = mysql.createConnection({
    host     : '39.107.103.65',
    port:  3306,
    user: 'root', //数据库名
    password: 'wallet201803', //数据库密码
    database : 'wallet'
});
//打开链接
client.connect();

//创建连接池
var pool = mysql.createPool({
    host : '39.107.103.65',
    port : 3306,
    database : 'wallet',
    user : 'root',
    password : 'wallet201803'
})

//查询SQL示例
exports.examples = function(){
    client.query('select * from WLT_T_TRADE',function selectCb(err, results, fields) {
        if (err) {
            throw err;
        }
        if(results)
        {
            console.log('查询结果长度：' + results.length);
            for(var i = 0; i < results.length; i++)
            {
                console.log("%d\t%s\t%s", results[i].id, results[i].name, results[i].age);
            }
        }
        client.end();
    });
}

//查询SQL示例
pool.getConnection(function(err,connection){
    if(err){
        console.log('与mysql数据库建立连接失败');
    }else{
        console.log('与mysql数据库建立连接成功');
        connection.query('select * from WLT_T_TRADE',function(err,rows){
            if(err){
                console.log('查询数据失败');
            }else{
                console.log(rows);
                pool.end();
            }
        })
    }
})
exports.client = client;